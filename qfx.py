import queue



class qfx(queue.Queue):
	def __init__(self,maxsize=0):
		super().__init__(maxsize=maxsize)
		self.qs = []
		self.indatacounter = 0
		self.outdatacounter = 0
	def put(self,item,block=True,timeout=None):
		self.indatacounter += 1
		super().put(item,block,timeout)

	def fx(self,item):
		#item = item #This a buffer
		self.outdatacounter += 1
		for q in self.qs:
			q.put(item) 

	def connectQ(self,Q):
		if Q not in self.qs:
			self.qs.append(Q)

	def get(self,block=True,timeout=None):
		return super().get(block,timeout)

	def flush(self):
		while self.empty() == False:
			sample = self.get()


