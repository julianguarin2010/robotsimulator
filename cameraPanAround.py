import bge.logic as lg
import mathutils
import math



scn=lg.getCurrentScene()
robotCamera = lg.getCurrentController().owner


#print ('LookAt:',robotCamera.localOrientation)

#Check if an action is been triggered for camera rail
joy = robotCamera.sensors['JoyButton']
joyButtonEvent = lg.joysticks[joy.index].activeButtons
railOn = (8 in joyButtonEvent) or (9 in joyButtonEvent) or (10 in joyButtonEvent) or (11 in joyButtonEvent)

if railOn == True:
	trackingObject = scn.objects['Arm01']
	trackingObjectPos = trackingObject.worldPosition
	robotCameraPos = robotCamera.worldPosition


	if (8 in joyButtonEvent) ^ (9 in joyButtonEvent):
		pos		=	robotCameraPos - trackingObjectPos
		e		= 	pos.normalized()
		zoomOut	=	e*robotCamera.localOrientation
		if (8 in joyButtonEvent):
			#Zoom In
			robotCamera.applyMovement(-zoomOut,True)
		else:
			#Zoom Out
			robotCamera.applyMovement(zoomOut,True)


	if (10 in joyButtonEvent) ^ (11 in joyButtonEvent):
		
		#translation
		deg2rad			= 0.017453292519943295
		pos				= robotCameraPos - trackingObjectPos
		rotFrom			= math.atan2(pos.y,pos.x)
		posxymagnitude	= mathutils.Vector((pos.x,pos.y,0)).magnitude
		
		if 10 in joyButtonEvent:
			rotTo = rotFrom - deg2rad*1
		else:
			rotTo = rotFrom + deg2rad*1

		pos.x = posxymagnitude*math.cos(rotTo)
		pos.y = posxymagnitude*math.sin(rotTo)

			
		localTranslate = trackingObjectPos + pos - robotCameraPos
		robotCamera.applyMovement(localTranslate)

		#Camera Orientation
		
		#The right side of a camera 
		cam_right__x	= mathutils.Vector((-pos.y,pos.x,0.0))
		cam_right__x.normalize()
		
		#The look at vector
		cam_lookAt_z	= -pos.normalized()
		#print(pos.x,pos.y,pos.z," <> ", cam_lookAt_z.x,cam_lookAt_z.y,cam_lookAt_z.z)
		
		cam_up_____y	= cam_right__x.cross(cam_lookAt_z)
		newlocalOrientation = mathutils.Matrix((cam_right__x,cam_up_____y,-cam_lookAt_z)) 
		newlocalOrientation.transpose()
		
		#colx = mathutils.Vector((newlocalOrientation[0][0],newlocalOrientation[1][0],newlocalOrientation[2][0]))  
		#coly = mathutils.Vector((newlocalOrientation[0][1],newlocalOrientation[1][1],newlocalOrientation[2][1]))  
		#print (colx ,cam_right__x, ' X ' , coly , ' = ', colx.cross(coly))
		robotCamera.localOrientation = newlocalOrientation
		
				
		


