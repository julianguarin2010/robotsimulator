
import sys,os
import qfx
import socket

try:
	from nanomsg import wrapper as nn_wrapper
except:
	print ("Installing Nanomsg")
	sys.path.append('/Users/julianguarin2010/anaconda/envs/py3k/lib/python3.3/site-packages/nanomsg-1.0-py3.3-macosx-10.5-x86_64.egg')
	from nanomsg import wrapper as nn_wrapper
import nanomsg


class nanoserver(qfx.qfx):

	@property
	def ip(self):
		return self._ip
	@ip.setter
	def ip(self, value):
		self._ip = value
	@property
	def port(self):
		return self._port
	@port.setter
	def port(self, value):
		self._port = value
	
	def __init__ (self, ip = socket.gethostbyname(socket.gethostname()), port = '5555',  maxsize = 0):
		
		print ("\nInicializando SERVIDOR en modo: REPLY")
		print ("Escuchando clientes en modo:  REQUEST")
		self.url = 'tcp://'+ip+':'+port
		self.urlb = str.encode('tcp://'+ip+':'+port)
		print ("\n\tIntentando abrir socket y escuchar conexiones entrantes @ url: ", self.urlb)
		self.socket = nn_wrapper.nn_socket(nanomsg.AF_SP,nanomsg.REP)
		print ("\tSocket: ",self.socket)
		try:
			binding_result = nn_wrapper.nn_bind(self.socket,self.url)
		except:
			binding_result = nn_wrapper.nn_bind(self.socket,self.urlb)
		print ("\tRecibiendo conexiones: ",binding_result)
		self.reqrepAgent()
		self.close()

	def reqrepAgent(self):

		result = -1
		while (result==-1):
			result,abuff = nn_wrapper.nn_recv(self.socket,1)
		
		
		abuff = bytes(abuff)
		abuff = abuff.decode()

		abuff = '<  **->'+abuff[0:len(abuff)-1]+'<-**  >'
		print(abuff)
		print(abuff.encode())
		nn_wrapper.nn_send(self.socket,abuff.encode(),0)

	def close(self):
		nn_wrapper.nn_close(self.socket)




if __name__ == '__main__':

	ip_host = socket.gethostbyname(socket.gethostname())
	ip_host = '127.0.0.1'

	default_port = False
	try:
		port = sys.argv[2]

	except:
		default_port = True
		port = '5555'
	print ("\n\n\tNanoserver test. By Julian Guarin 2015.\n")
	print ("Using default TCP Port: ",default_port,"\nStarting server at: ",ip_host,":",port)
	print ("\n")

	servidor = nanoserver(ip_host,port)
	print ("Ok bye")






